import React  from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import { testMapPoints } from './Datas'
import L from 'leaflet'

const DEFAULT_LONGITUDE = 36.74564170000001;
const DEFAULT_LATITUDE = 3.0697775;

export const suitcasePoint = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/PaulLeCam/react-leaflet/master/example/assets/suitcaseIcon.svg',
    iconRetinaUrl: '../assets/suitcaseIcon.svg',
    iconAnchor: [20, 40],
    popupAnchor: [0, -35],
    iconSize: [40, 40],
    shadowUrl: '../assets/marker-shadow.png',
    shadowSize: [29, 40],
    shadowAnchor: [7, 40],
})

function RenderMap() {


    let nowArr = testMapPoints.attractions.map(cordi => {
        if(cordi.types.includes("museum"))
            return (<Marker position={[cordi.lat, cordi.lon]} icon={suitcasePoint}>
                <Popup>{cordi.name}</Popup>
            </Marker>);
        else {

            return  (<Marker position={[cordi.lat, cordi.lon]} >
                <Popup>{cordi.name}</Popup>
            </Marker>)
        }
    });
    return (
        <Map center={[DEFAULT_LONGITUDE, DEFAULT_LATITUDE]} zoom={9}>
            <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {nowArr}
        </Map>

    );
}

export default RenderMap;

