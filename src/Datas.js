

export const testMapPoints = {
    "locationName": "Algiers",
    "locationId": "0",
    "attractions": [
        {
            "business_status": "OPERATIONAL",
            "lat": 36.74564170000001,
            "lon": 3.0697775,
            "name": "The shrine of the martyr",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJVes1dZ2yjxIRPKvH9FziPJM",
            "google_id": "906dbf5b5faf6a618c483643b806af265ce956c1",
            "photo_attribution": "906dbf5b5faf6a618c483643b806af265ce956c1",
            "google_rating": 4.4,
            "google_ratings_total": 2563,
            "vicinity": "Chemin Omar Kechkar, Algiers",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/110185357512444748478\">Sa\u00efbou abdoulahi</a>"
            ],
            "image_reference": "CmRaAAAAhed7hWBWT4ZVpSxUeVsBjfmTwXR8X2b1gyM6CC02IOavBuhXi84x2MhspKOiM65HDNw26jGJizNr_84QblP5KIYeK8s-JgVKHESifTKSHgvE-FvKvRGcNU4N9DhM_aEiEhCkM8q9LEKShDSDab82ty26GhRqAH0lpS52j15BUUUqp5pJ9oE6aA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7461113,
            "lon": 3.0723001,
            "name": "Museum of Fine Arts",
            "types": [
                "tourist_attraction",
                "museum",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJbzZuJJyyjxIRSEWBr_Y6jhk",
            "google_id": "9f2944a582164e130851312ce5428e2e2896797e",
            "photo_attribution": "9f2944a582164e130851312ce5428e2e2896797e",
            "google_rating": 4.6,
            "google_ratings_total": 234,
            "vicinity": "Rue Du Dr Laveran, Algiers",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/114500391902832511182\">Professional Network Marketing ProTeam MLM</a>"
            ],
            "image_reference": "CmRaAAAAP4eRwsM8UD4SmqUPW0lco3DiLDA-PeJcFnNvKVNwe7-R2h8Ru953lQaCpeZ8uPs1k4II5kjjWKNzPwNeB6IJLDlVYM3CDGghRQo9lw_XT0vFpSyiV1iuOv4BV2Y0z0J6EhAQWx56LLp88QyphM2BnpccGhSXs3XyNJyvVzYyCV7SUeILAfjhWQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7922684,
            "lon": 3.2349654,
            "name": "Marina Palm",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJFRMpdxJPjhIRCkNcoUp0RAQ",
            "google_id": "e9c10da68db80dca273a4f5c41ae7a9685a4f6f6",
            "photo_attribution": "e9c10da68db80dca273a4f5c41ae7a9685a4f6f6",
            "google_rating": 3,
            "google_ratings_total": 116,
            "vicinity": "Bordj El Bahri",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/114364855546648548236\">Reda Bourayou</a>"
            ],
            "image_reference": "CmRaAAAA9dJalVEJSYIpd0u0Ft7uu0M-pycv11yg6P35VHH0ogWrwE6OaxgVlp4gQti8NuS5sj-xJTde3ExPzkseM5Mg7SLPykPWeG1v6JHvEoe8SDHEY4cvas50iJlyJypq3L6DEhAXtZXXImW0zJA4aZSqfe8wGhRnMxaH9QUr1-N829A_FNB1HyMrxQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7856718,
            "lon": 3.060808199999999,
            "name": "Dar Mustapha Pacha",
            "types": [
                "tourist_attraction",
                "museum",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJtxG6J_6yjxIRLf-sqIsln-U",
            "google_id": "b01a7edb924f6d912edcd62a2a3cbed7f6c9ec49",
            "photo_attribution": "b01a7edb924f6d912edcd62a2a3cbed7f6c9ec49",
            "google_rating": 4.5,
            "google_ratings_total": 65,
            "vicinity": "Rue Aoua Abdelkader, Casbah",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/115346079167635185193\">MARNAS Officiel</a>"
            ],
            "image_reference": "CmRaAAAA88Xo4Id9A9k9v9EZJMks5pLr1pGPJaAUlT3MQMLV8A4N2zalFK4R4fmSJwrNEDwXCvD-KQ-BvmJ7wifWJZtPwpPJtox2Wf-sH5e8jNwL7j7RS9hFSsrVwTZL7bQ_IHsSEhDEC1UwNCTErt9CA6VAOs77GhT7Yq78Gs3oMfc8BGaB0BZVis25lg"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.80092620000001,
            "lon": 3.0426368,
            "name": "Basilique Notre Dame d'Afrique",
            "types": [
                "tourist_attraction",
                "church",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJu0874LuzjxIRZw7ZrxzdAz8",
            "google_id": "1a1d71d195bcb8d4cc90f672505c97e766e7a51b",
            "photo_attribution": "1a1d71d195bcb8d4cc90f672505c97e766e7a51b",
            "google_rating": 4.5,
            "google_ratings_total": 1110,
            "vicinity": "Rue de Zighara, Bologhine",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/117171413083532674395\">Hocine Boudjemaa</a>"
            ],
            "image_reference": "CmRaAAAA9YpWXyQEACH6rmYpsI-yQjINKc3GBQnVjwNf72MGqqaqcGkmLfvNVOedr6CBLI15lKHi7RUcYjuy9GKqP6eDV_gjl7x7x-7eqNRH0P7aVHO50lw2Jr9YVq6_iqubOOO-EhDi8_4-ybEtmk-muaApsbhEGhTJFSr0UgUzNw1oaAL8PliQnwmPfQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7331167,
            "lon": 3.0552845,
            "name": "Art Gallery Benyaa",
            "types": [
                "art_gallery",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJJdDU3oKtjxIRVvwED3nRZoY",
            "google_id": "574bc6334fe83a7084e7e753d45e2ec3afe482cc",
            "photo_attribution": "574bc6334fe83a7084e7e753d45e2ec3afe482cc",
            "google_rating": 3.9,
            "google_ratings_total": 9,
            "vicinity": "Cit\u00e9 Les Sources, Bir Mourad Ra\u00efs",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/111693506043416981447\">Djamel Benyaa</a>"
            ],
            "image_reference": "CmRaAAAA2IQn4Q7Ns2VXSRmUUWDH5NqUBwK5qN5yzcGzeIN5ICrP7bXwgkZqLEiIRmYLxQ3cmtbIytRCpQECQS6Y5plwUINuD-PlbN_E343Np3SeZleDpe5f55eSUSnQ4QxrIGN8EhDpbaleEhCbD5XuD75jt6p2GhT3PkBRxf5q0HVOCsx1V1wg_BbWLg"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7378322,
            "lon": 3.0322715,
            "name": "Al Marhoon Gallery",
            "types": [
                "museum",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJm_u5sPStjxIRnzfNcDuedO4",
            "google_id": "0e4954a68da2cf1027382c98ab7a6d2e4c8fa7a8",
            "photo_attribution": "0e4954a68da2cf1027382c98ab7a6d2e4c8fa7a8",
            "google_rating": 4.2,
            "google_ratings_total": 11,
            "vicinity": "Cite 574 logements said hamdine bt 24 local 12, Bir Mourad Ra\u00efs",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/100541963169707824824\">Ahmed Ait issad</a>"
            ],
            "image_reference": "CmRaAAAAFycydH5coSLNEC1xatwpbAhWqjVD6ZvLL__-5kGE1WodlSaHPkYwogcspOJVtnedTRWvqvsi6XeZJnRbQqHPK-rnNrTDe91lHYq0X1TPyg1xCuPhGd6P-U3wE46lg3KVEhBj5hwhKNwr3SvqKFcRKhJoGhTkYY84cTf5gvasXwsbZiK8IG-JHQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.78673009999999,
            "lon": 3.0609627,
            "name": "National Museum of Arts and Popular Traditions",
            "types": [
                "tourist_attraction",
                "premise",
                "museum",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJW9S5jgGzjxIRTWzoldeHXg4",
            "google_id": "fe17e1e56b9d01661853aa3c3c039a3316c0269e",
            "photo_attribution": "fe17e1e56b9d01661853aa3c3c039a3316c0269e",
            "google_rating": 4.1,
            "google_ratings_total": 88,
            "vicinity": "9 rue Mohamed Akli Malek\u060c \u0627\u0644\u0642\u0635\u0628\u0629",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/108978648858208949576\">David Clarke</a>"
            ],
            "image_reference": "CmRaAAAAzdB5CH0XhG8Nk1aVaXK0SHiPZeTRMSuJgMEFhvCwU13UpiBX5BK1mYbrAh6Ehhm-1TR-t4sQ1Lu80wP95UWhXV7Yb7CH46uDV7Sn44Pzhc0LdlC0Pa1X4izOGHITbQ2TEhC89VsPXD-32f0Wfxgg_KRMGhQcqnXdfcIyqbXkKsxHY1WDjY4wkw"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7884482,
            "lon": 3.0639383,
            "name": "Palais des Rais",
            "types": [
                "tourist_attraction",
                "museum",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJV2vASgKzjxIRbpsAbikrRIg",
            "google_id": "7ec240fdfaca4769e7ed70f974873349f20f3b2c",
            "photo_attribution": "7ec240fdfaca4769e7ed70f974873349f20f3b2c",
            "google_rating": 4.6,
            "google_ratings_total": 272,
            "vicinity": "23, boulevard Amara Rachid Basse, Casbah",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/114877955251383486902\">Makaveli Mistick</a>"
            ],
            "image_reference": "CmRaAAAAAvinPOu2BTzLrC9bwJw3X2NF_4FFvH_nB_zafLAk5E8kaJhY3M91_zEAGU10vgPFjJpKEHeTFvepXDrMKfX4wMresCEcRLkDRlHVK4gJ0wgAG0DM1wNivIdYe4mC8sy9EhBLl5MBOfWi9yD0e7bjEuq5GhSXhuHxZ2MBlcM-F8ii3Q-qd-BqUA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.74222220000001,
            "lon": 3.1180556,
            "name": "Sablette Seaview Ballade",
            "types": [
                "park",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJSY0up19NjhIRxfY4EKw7kvA",
            "google_id": "bab4df150e778213e2c1e2abaa1c11406733e940",
            "photo_attribution": "bab4df150e778213e2c1e2abaa1c11406733e940",
            "google_rating": 4,
            "google_ratings_total": 625,
            "vicinity": "N11, Hussein Dey",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/100868558525160572308\">Nabil Ami</a>"
            ],
            "image_reference": "CmRZAAAAV3md3z2Ni6ke4HeqQ-bHsz5tn-dg49iCaZidjoLC_l488MGinHZMJkvbZz-gY7D31GBIyt_QhwQERi5tpj11PADsyKgtE0078EPpJhR4dqfJOd_pUfAX6ym44Cm3Ogm5EhDXLBIw6Nbj_Rshb0oZeZ3gGhSUxMFHFc02fBc0pq1YjkGR4YQV8A"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7573757,
            "lon": 3.219777300000001,
            "name": "Piscine Aquafortland",
            "types": [
                "tourist_attraction",
                "spa",
                "amusement_park",
                "restaurant",
                "food",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJTR70HctPjhIR79-qa_sAMUg",
            "google_id": "4ff8b8596027d39b40fc5721f40ea30a8a28caf5",
            "photo_attribution": "4ff8b8596027d39b40fc5721f40ea30a8a28caf5",
            "google_rating": 3.9,
            "google_ratings_total": 406,
            "vicinity": "56 Route Nationale 24, Bordj El Kiffan",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/109061234392347986197\">Piscine Aquafortland</a>"
            ],
            "image_reference": "CmRaAAAAyqEHBsk9s-zsei56gHF_XGMqv980P2uXyAo-gVLLaLCaTrP0TfT1Lts3GzFk2SC1xMbSFramLgKb5WM4PLCIPEQKXeUGM8KoTmqU3MS5LAltI1cO9I7lsctZvS_Jo33lEhCdRTBfcuifwe7Q_12XPsOEGhS7iSgUcEUq4TUz-zPnSuW8UoWzvA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7853277,
            "lon": 3.0641195,
            "name": "Grand Mosque of Algiers",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJqeMFjP2yjxIRMrcboaXzlmk",
            "google_id": "39cc6bde332fc259ba3122f11b6428f1e77adee5",
            "photo_attribution": "39cc6bde332fc259ba3122f11b6428f1e77adee5",
            "google_rating": 4.5,
            "google_ratings_total": 259,
            "vicinity": "Rue El Mourabitoune, Casbah",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/110017435136742484179\">king 77</a>"
            ],
            "image_reference": "CmRaAAAARguazHIy8ST8-oV1fvj-CU0XzrEAfnnfDRPHFvMDCxww0yDXG89_rvvqKgvxxJp7CDT4Mplr1QVwCHRxG_Y5zglKZTSaiMb5_vhEL3WbAJI9RWLQrhefFkMo_nLl-XxjEhAnXeutujZ-Yn_uGqxouVkEGhQCs_yJ3bJnUfj-_5NtsSy3Ge147Q"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7850402,
            "lon": 3.060808199999999,
            "name": "Mosquee Ketchaoua",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJgXehOv6yjxIRRwuxjEdaDvU",
            "google_id": "d7cb0bdd905c3303b5b308851a36440d9d5ad586",
            "photo_attribution": "d7cb0bdd905c3303b5b308851a36440d9d5ad586",
            "google_rating": 4.6,
            "google_ratings_total": 548,
            "vicinity": "\u0627\u0644\u062c\u0632\u0627\u0626\u0631",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/110227667278285789034\">Omar Hamdi</a>"
            ],
            "image_reference": "CmRaAAAA_tpQjYNlwoJ--_0cax-o3leRk0B0o5uqMt_K8vlfcKWjZcMHX4_BWkFABre4QkdGpigvUTky-1Dj_BnmMyrkTdDnS16v66yagJwjUDdmcFDV8WNyL9sSLpO_HsmDQ77CEhBBV3k6DTZzF53Jd6NIY76lGhTflrF82Zj6WAwFTUVLNKx1LZOPDQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7641023,
            "lon": 3.0474481,
            "name": "Sacred Heart Cathedral",
            "types": [
                "tourist_attraction",
                "church",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJtUXiBmiyjxIRh78pSh8qvmo",
            "google_id": "c216a0bfc36ad1c2c07f4cd0f9f883872aa10c7f",
            "photo_attribution": "c216a0bfc36ad1c2c07f4cd0f9f883872aa10c7f",
            "google_rating": 4.2,
            "google_ratings_total": 211,
            "vicinity": "Algiers",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/104551590510705221080\">Samy Chabane</a>"
            ],
            "image_reference": "CmRaAAAAwujblQ6KA0kVWuuG829DIkaE9FsASAEiipFiAgEjKRf6Vi5rrthbxN3dwmFZCW_nZQhtQevFa8C2WKiFxKCZs0O2FhSwNVOG0cGc2CLx1KnrKL5tPyumW5sD081BnarGEhBqms1ZONQU3U2B0t06zIZeGhTBZATVaeiruDPjCC6fGe_poaQ9Ng"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.74840429999999,
            "lon": 3.075777999999999,
            "name": "Botanical garden El-Hamma Jardin d'Essai",
            "types": [
                "tourist_attraction",
                "park",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ8du20ZiyjxIRLqnQ7p7GxYA",
            "google_id": "bfccf5f1b0224672291a51be7a4ff6ea171344cd",
            "photo_attribution": "bfccf5f1b0224672291a51be7a4ff6ea171344cd",
            "google_rating": 4.5,
            "google_ratings_total": 2434,
            "vicinity": "Rue Mohamed Belouizdad, Belouizdad",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/101454699701051347809\">Mehdi Benboubakeur</a>"
            ],
            "image_reference": "CmRaAAAAmDOoOeLDSXYwpvEGCAjSkWMqpbROUBaS2iu1OgXTTwln0oig8-d5kVbDBJ-IwHlqH36gm4iQyfgbtw37i1w6kpKuCTcjZjhAeHMj5_HfLovwdiiqKaV-pdbksqia11IJEhCbGBmWTD-zcHGvyQxyO7ztGhQnJh4AYUF1Uea658-KgGZbHTUYwA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7607844,
            "lon": 3.0471554,
            "name": "Bardo National Museum of Prehistory and Ethnography",
            "types": [
                "tourist_attraction",
                "museum",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJBb3owGiyjxIRaOCcMbN-ZNg",
            "google_id": "e9a51814f794c8eefd15b238a535e6abf0d567ea",
            "photo_attribution": "e9a51814f794c8eefd15b238a535e6abf0d567ea",
            "google_rating": 4.4,
            "google_ratings_total": 366,
            "vicinity": "03, Avenue Franklin Roosevelt, Sidi M'Hamed",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/112311127620591648160\">Bardo National Museum of Prehistory and Ethnography</a>"
            ],
            "image_reference": "CmRaAAAAs9sx8Fpy-qlwra5CwLwmMZq9otxLpYxkIuKSDiiyWOvvHjVggv6IwEo7JMP79WkVwuzFLbxawJ0hJIykVqUyr-d3QEIUrI2A1GIGY9YqvW21fdttNjbv95zMZ7Cq-UcmEhAMW4ZnnbpdMiM1tKKZhlCpGhSVZPq32LLt3amXhthQubkNAik0FA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.78458,
            "lon": 3.0624425,
            "name": "Martyrs' Square",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ0aNHCv6yjxIRwOCawOfbpo0",
            "google_id": "f2ec032cb6b1b6b74191c6dfa20e3764978dc31a",
            "photo_attribution": "f2ec032cb6b1b6b74191c6dfa20e3764978dc31a",
            "google_rating": 4.4,
            "google_ratings_total": 858,
            "vicinity": "Boulevard Amilcar Cabral, Algiers",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/103078084738911184402\">Moh AD</a>"
            ],
            "image_reference": "CmRaAAAAGZTP0xkCnSIjaaLzqIQm6rprDTAllpRQoqlkgr2uXftwIBqpoiZbtfXClcx2D8_6cIfF9_8Twql2Q7XTk4bKG_5UlABegYR7xZ5owB2rKpCvLYVIdeQL3G5aRBIzFcDKEhBQvDBjhmNKXPT2M0CjHGUiGhSOSrGxNjGBTr5iz1a4HBAYLzFJKw"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7508634,
            "lon": 3.0050062,
            "name": "Zoo and entertainment - Ben Aknoun",
            "types": [
                "zoo",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ4THlbeKxjxIRtb9gDevRq44",
            "google_id": "7f8bac402f7c7f63ded415f3264d218b873cebb9",
            "photo_attribution": "7f8bac402f7c7f63ded415f3264d218b873cebb9",
            "google_rating": 3.5,
            "google_ratings_total": 719,
            "vicinity": "Ben Aknoun",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/109822148804361708727\">nafaa dz</a>"
            ],
            "image_reference": "CmRaAAAAumlSHRp69iGmxnCHlOU4YQuVVK3lV2RVua5TeKROoJFnzFrSwYcwEufRPtA4xoIrsK4z4XWYA7qwoHYzHAWAlbh8YmjAkn3W_sDtxl9kbMHQ6ZXQCE1yHwHvVQqoeFpjEhB02baoWJPXsPTyAUPsqfRcGhQZejScsgXn4dYrKux_cUuHyaJ1bQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7773012,
            "lon": 3.0575721,
            "name": "Museum of Modern Art of Algiers",
            "types": [
                "tourist_attraction",
                "museum",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJOZKv1VeyjxIRcoFpiVdSkmk",
            "google_id": "9ab6388956f4ad1862467b360d2ed00ac03237b7",
            "photo_attribution": "9ab6388956f4ad1862467b360d2ed00ac03237b7",
            "google_rating": 4.2,
            "google_ratings_total": 154,
            "vicinity": "25 Rue Larbi Ben M'hidi, Alger Centre",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/117017000988493496781\">Yannick Portier</a>"
            ],
            "image_reference": "CmRaAAAArobyZO9P-qHGKp8QtAsxzOVCNRllPgb1d3fp7-GaB95pwumr7OulLU90FKdvto_HEp5IjwETjSVrSxAxHjf8-j4arY1OUEsuhZEBwAVyB4MKasy-mVQNHTB2KS7FF9IOEhAI2xzJSf8FDFxt3AgJ3xwtGhQd-OYBys-FcYLn6xucYYC942WQIw"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.73581310000001,
            "lon": 3.1602967,
            "name": "\u062d\u062f\u064a\u0642\u0629 \u0627\u0644\u062d\u0644\u0645",
            "types": [
                "amusement_park",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJvSi1eQtSjhIR7SM16aDoKhg",
            "google_id": "23d87dbf11d19b722a165d999ab8988ea73c37fa",
            "photo_attribution": "23d87dbf11d19b722a165d999ab8988ea73c37fa",
            "google_rating": 3.6,
            "google_ratings_total": 541,
            "vicinity": "Mohammadia",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/108980275584610030495\">EL-Gamah Med Riadh</a>"
            ],
            "image_reference": "CmRaAAAAFE64sq3OkvSKB07WX5NG9OHp7AugB7y5n-1a1p78x0EeAA-xrLZ3C-k_ZQsexVdClqhvLv4sL7Geg17wFilVLDWbSGoOV4sDt7co5xSW9AcI4iRKjpdrZ2KQvxvI1p0TEhBMFyvrnkM1GtyOwyvlimE7GhRSw4ob1ehkWATm9jexxF6fiwSDbQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7809046,
            "lon": 3.0612197,
            "name": "Port Said garden",
            "types": [
                "park",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ5z7_yP6yjxIRzOTk15IHkYA",
            "google_id": "e63d56c60b4ce1a632766b001a578c2d85daad87",
            "photo_attribution": "e63d56c60b4ce1a632766b001a578c2d85daad87",
            "google_rating": 4,
            "google_ratings_total": 298,
            "vicinity": "19 Rue Abane Ramdane",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/115820346708265301071\">KhaldiDZ Hamid</a>"
            ],
            "image_reference": "CmRaAAAAc7dUCqRmU3yIQOvGL4DXPmMcQvMUXO_YwqHpd_7ULwGPJ4p8neiEA6jEnxbC4uZq0TNTw7L-CXdLx1fGsFUOj1p_MvEDwejl4YP1MmSr9fLIBs3f0RffAMSF7Xy0GMw4EhCFfx7owlygr1uTX1cHZnmNGhQpOKudAgYKzVAhkhOFGj0qXY49FA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.76832720000001,
            "lon": 3.0466658,
            "name": "Beyrouth Parc",
            "types": [
                "park",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJoTYgO0OyjxIReUhseHo-38Q",
            "google_id": "f0fa86636a59a165101c394e1dfeb3a93ce6b44e",
            "photo_attribution": "f0fa86636a59a165101c394e1dfeb3a93ce6b44e",
            "google_rating": 4.4,
            "google_ratings_total": 152,
            "vicinity": "94 Boulevard Colonel Krim Belkacem, Alger Centre",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/110482041555065588627\">Reda Brikci</a>"
            ],
            "image_reference": "CmRaAAAAGMgpU2zJYoRzS3JehoOgm2qZXqLY17CugLX8P3al5kLXuruiMQ_yUB6NyBNjrYs2mQJeQElya8UFGRzUhfNgxwu1vybig4P-KsCwr-Vg76vQ9hkZxjzmSaffiyTmmb_iEhCcml76wkO9m2BsIBr6-fL2GhTmNgxlL2NGtjQEQ7CXtPOfk4TeRQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7762116,
            "lon": 3.0583627,
            "name": "Emir Abdelkader Place",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJYYVFEviyjxIRfHfe-eo4Fvg",
            "google_id": "f524d7a1336bddc3263501333c22b1fcf146c9d2",
            "photo_attribution": "f524d7a1336bddc3263501333c22b1fcf146c9d2",
            "google_rating": 4.4,
            "google_ratings_total": 377,
            "vicinity": "Rue Larbi Ben M'hidi, Alger Centre",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/105851455830862154308\">Gloria Gravina</a>"
            ],
            "image_reference": "CmRaAAAAkYYerK7CD4AS5LZa7DBDmZEUpLorPG7Ii5Ig5uUWO80NMnfB3aBzuxPGpOI5qSy1cN2LhWJ3QlmJNUqrQ42Lauqxn62JhZXSd3B54ZvYCU2ufZRG1sBSB323I-p22nAjEhAOJslYgbXbA1YqnGny8tRvGhQxkOKZZjO5B0FySKc8gDNyHJjNDA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.76144290000001,
            "lon": 3.0461964,
            "name": "National Museum of Antiquities and ancient Islamic art",
            "types": [
                "museum",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJGZ1l9GiyjxIRM_ieIaqv62U",
            "google_id": "62885bd4c5666cf363c2a662e371d01dbaf511a0",
            "photo_attribution": "62885bd4c5666cf363c2a662e371d01dbaf511a0",
            "google_rating": 4.3,
            "google_ratings_total": 137,
            "vicinity": "03 Rue Franklin Roosevelt, Alger",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/117549653446428435083\">Kerba Ahmed</a>"
            ],
            "image_reference": "CmRaAAAAZC_KJBRDXvJ3WfmIcdRuFt7EyRkXHUNyPYViQUIlmzkxnBW-3CWUvgg7HVVaUboUK1HRP6cnfqjewFloOwpMNW5vGa6riXgajOFa11UnF9BKfQpAF0uh3mokgw3-d7m-EhBmOTjgMS--qug3fcK4gxM8GhTl5NHCRfxzoeB_8uG5f8_K6woE_Q"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7665394,
            "lon": 3.0540987,
            "name": "Al Rahma Mosque of Algiers",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJJ7S2RF6yjxIRY-I0faz62_c",
            "google_id": "7a13317b594143b5ef7cd9501cfb9992042f726b",
            "photo_attribution": "7a13317b594143b5ef7cd9501cfb9992042f726b",
            "google_rating": 4.6,
            "google_ratings_total": 204,
            "vicinity": "Sidi M'Hamed",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/106776270431063428525\">Islam Kriss</a>"
            ],
            "image_reference": "CmRaAAAAYgqLHu3zby42rWska7YbCn97B05R-CPQnTa4SEj95d8lAsUUEd2UZSg8t0m7UH6CmdpyzsxoDYRgmqt-aLXmq0K9i1OCAqFXrM0szl-4UXiMvxxNAc0P2zjHG1UkehKzEhCN9YEQSRGFLjQlG4BDzQUfGhQLu2oNDaEgLBW92-sNsmVhYZs38A"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.76246019999999,
            "lon": 3.0458834,
            "name": "Freedom Park",
            "types": [
                "tourist_attraction",
                "park",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJc7_dWmiyjxIR7XJXpMkuJ6o",
            "google_id": "8c9dff40dba0d2815b4e0a58961927078f7cff1a",
            "photo_attribution": "8c9dff40dba0d2815b4e0a58961927078f7cff1a",
            "google_rating": 4.4,
            "google_ratings_total": 161,
            "vicinity": "Rue Didouche Mourad",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/104079483162635382731\">Oussama LABBIZE</a>"
            ],
            "image_reference": "CmRaAAAA6bqlvKOew4ZbFCwP_p_ZZnTE2XcDJV4E67-9PEAO8xM3b_eXiVaWVCVmp2mNB1RYAaWM1dWM8HN904yiYIbKOoJIrcIX1XqjMvKj1Tjsn86vXFu97bd1bo-x3Juwq7o-EhA70HQnZdKfAFoKD5U8Hb6-GhT6Zol41Dcg9KQFvnKJpoabNeGurg"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7432098,
            "lon": 3.069770399999999,
            "name": "\u0645\u062a\u062d\u0641 \u0627\u0644\u062c\u064a\u0634",
            "types": [
                "museum",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJbZ2kcJ2yjxIRo6vzb9cRHKo",
            "google_id": "196ae69a3ce36dc31b0a3b805e6e04716f847be3",
            "photo_attribution": "196ae69a3ce36dc31b0a3b805e6e04716f847be3",
            "google_rating": 4.6,
            "google_ratings_total": 55,
            "vicinity": "El Madania",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/107326187128160670659\">SAMI GR</a>"
            ],
            "image_reference": "CmRaAAAAyXO5zOA8zypUP3OuqD_lm_S-yyRG9SiiIX6CsUP85Z6vgSoG29I6vc2EEUxLxaVXnLehuGr7pqN3Pl-jBG-z4D0pyjZbpcO0ddIHa2jeIdZYXtp2xSZJK_xde7jG6ycGEhBXAR8l6dcZr7hYpWphBA9AGhQ_lWGiH1I0o9bC5eeJGc7xyRcr4g"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.74290789999999,
            "lon": 3.070134699999999,
            "name": "Riad Al Fath",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ9Uk1eJ2yjxIRv8WR-9BFRwc",
            "google_id": "805a5344a98227fcd2d08a077225454ec045dc5a",
            "photo_attribution": "805a5344a98227fcd2d08a077225454ec045dc5a",
            "google_rating": 3.9,
            "google_ratings_total": 597,
            "vicinity": "Boulevard Khelifa Oulmane, Alger",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/110264357787004137376\">Youcef Zeggane</a>"
            ],
            "image_reference": "CmRaAAAAdJzMsmZdqTiNi418WmkNG_SDxVarTxmz9evjmkGgeB-5t-1snZs_eUR3bEeooJmCJglOpn_OYRXDGLdJh8Trji0WcDhyL2dPCGW8tqkbcRIDKceu3GVXvhB6rorgIruKEhAXX3owU-YcY56T_7syMa8_GhT2-aWPdmAZbDsOyK5B7i-1qKtUQw"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7448632,
            "lon": 3.072929599999999,
            "name": "Villa Abdullatif",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJB14VeZ6yjxIRZl0GjElDpW8",
            "google_id": "362ac3411219ceb58e737c5dc16a38f61dba68df",
            "photo_attribution": "362ac3411219ceb58e737c5dc16a38f61dba68df",
            "google_rating": 4.5,
            "google_ratings_total": 90,
            "vicinity": "Rue Du Dr Laveran, Algiers",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/110161611892742714587\">kati kiki</a>"
            ],
            "image_reference": "CmRaAAAAV8jRZnF7L-BtJonfTL1xVe1Wy-9s6SxEvNzSBBT6CGolEMCPbvionuBCZ2BiLqQwCTdK58jCqFCwSenogDu04k5F_p9EOXtZoh5udU2agu0J_86L1q_U9I4LmwAGF6cNEhBqMgb7cbF_L9Or12SMvwhhGhR178q5Wrnk8UMECN6Fgx4EUyRfNA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7418103,
            "lon": 3.0703138,
            "name": "Army museum",
            "types": [
                "tourist_attraction",
                "museum",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJtUErY2KtjxIRBdytF4pRnRI",
            "google_id": "15fb9cc981a59305490227d88e590bc452b783bd",
            "photo_attribution": "15fb9cc981a59305490227d88e590bc452b783bd",
            "google_rating": 4.5,
            "google_ratings_total": 49,
            "vicinity": "El Madania",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/113222998662894963793\">Tech Innovator</a>"
            ],
            "image_reference": "CmRaAAAAR5EvYWAROKNRtYe1AKjtVc74q1joBKgnrbHim3__G9vkP-nHpRrxGNXiZqjqWIavFNjK82lhU9YWJYHzRk3dsxTveueDSl7vYX8PgUWAwol19RluWAPEumqIiuAcmTQrEhCJ7lMFjZplkfYEBy1jkih6GhRiscZtp3jFUPx55hkJXAqRzfCE5Q"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7558892,
            "lon": 3.0312871,
            "name": "Aqueduct Ain Zeboudja",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJHzEQvhGyjxIRwGdZ7z7J8Gc",
            "google_id": "e05a2c56d20b5f020459105678acce1d315ff887",
            "photo_attribution": "e05a2c56d20b5f020459105678acce1d315ff887",
            "google_rating": 4.1,
            "google_ratings_total": 10,
            "vicinity": "Val d'Hydra, Hydra",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/114500391902832511182\">Professional Network Marketing ProTeam MLM</a>"
            ],
            "image_reference": "CmRaAAAAdoakdzewgbrYJg01TikR-AIup5P9F-uyi4Eh_YFLVTlAGoO-1_7V2qSx2ZXKvVDrOoNRD8jMKELbxNt0b206Upg81jT8HveSkOra6U5FyALXPx4BCbx8WkkOFdnFaCubEhA-cZCuBtklimLg-JF5VJD0GhRFzWkAUAa3oqlg-7PDen42qHL5mw"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.77936889999999,
            "lon": 3.0590862,
            "name": "Ibn Badis Mosque of Algiers",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJddk9wfiyjxIRGdT6VT4FhbQ",
            "google_id": "8d5d1aab418ff23ea83c1ba702eb1767d5ad04ce",
            "photo_attribution": "8d5d1aab418ff23ea83c1ba702eb1767d5ad04ce",
            "google_rating": 4.4,
            "google_ratings_total": 49,
            "vicinity": "El Djazair",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/112858486497973126927\">Kamel Bouremad</a>"
            ],
            "image_reference": "CmRaAAAAb0fh9xLMysezSWsbO2nVekwbdgxL4wUzU57mhm8ckqO6O6_AaVq-kvbhO_SxnKnLQgg8lgwsrgZ92KjrnxpwfFl_bC9WkpWOz2kbs4NtQUwQvkpoL4SsBvhB6vcRZuDyEhBIIR6mqcTlbfe7SiUGvf8AGhT6VfNvXiaN_ZeIbjjQ-Gq7SCO0QA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7813883,
            "lon": 3.061114,
            "name": "\u0628\u0627\u0628 \u0639\u0632\u0648\u0646",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJhXqxxv6yjxIRn3Fm-_k-fzc",
            "google_id": "ca2e00f3156c07abb53cfe0e4459d1b891149d80",
            "photo_attribution": "ca2e00f3156c07abb53cfe0e4459d1b891149d80",
            "google_rating": 4.3,
            "google_ratings_total": 151,
            "vicinity": "Rue Bab Azzoun\u060c \u0627\u0644\u0642\u0635\u0628\u0629",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/100954548212889720750\">Mohamed Nacer Abdi \u0628\u0627\u062d\u0645\u064a\u0627 \u0627\u0644\u0645\u062a\u0641\u0627\u0626\u0644</a>"
            ],
            "image_reference": "CmRaAAAAB3fx52dVRHMpjlRPnix527F1hB1DIRp-0zoPE_CkOrK_zActjOetL6tSuj0rknbIE21i-5q8HRkK0MImMjtFFBMqEiNsvtkhY3rsyRve8o0Gm8T6u2p_yWBFuxo-sgPzEhC7Vjobkd9FqWYpnGTnB_IhGhSGLA1AWftheOxe2uEqBLJH7j71Hw"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7832916,
            "lon": 3.057483,
            "name": "Safir Mosque",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ_VE0flWyjxIRpBRJ4-pRvNY",
            "google_id": "878bd0ac5985da36f465151c92e75fd2c1f989cd",
            "photo_attribution": "878bd0ac5985da36f465151c92e75fd2c1f989cd",
            "google_rating": 4.3,
            "google_ratings_total": 12,
            "vicinity": "Rue des Fr\u00e8res Bachara, Casbah",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/116519734824192753953\">Yamina Miri</a>"
            ],
            "image_reference": "CmRaAAAAA0mTA7cI06gcueP37-NZxIQgjNAciXcK9_BHE_ljfiR_xOwE67E_3LZOYF2qj4Exg61_JU72NJba8LA5cxxXEeKiJCCdTRPzgRBheyER01BOKY_I3Yr01zrdHWJ_1xt2EhD0OivJjnYtq2IuFS0YaeX6GhQLUKiCLjVvftG1kh-wUWpw6y4RxA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7837191,
            "lon": 3.057962400000001,
            "name": "Sidi Mohamed Sharif",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ833Qhf-yjxIR25nn2RH3Jp4",
            "google_id": "6539d727910d8031a370ec4c8c5638dc145e6910",
            "photo_attribution": "6539d727910d8031a370ec4c8c5638dc145e6910",
            "google_rating": 4.6,
            "google_ratings_total": 18,
            "vicinity": "Rue M'hamed CHERIF, Casbah",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/115093902587575483567\">halli belkacem</a>"
            ],
            "image_reference": "CmRaAAAA_BBajxcBeosng3IZN57FgQ2M09ltKrEOpuY4xiifgV7DiN6MbhdpUg6GyuKBGWkL5mTdupExa1TDGex924-yOJ4b8j1fvBTj27Tpk7VuJCNjBR-0iMX6BaODvGOtNdxxEhClh2FUzcgOh1ourXfSVdiaGhShO1T7vD4bLx39zZ9wMO5QWsqSgg"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7851092,
            "lon": 3.058724100000001,
            "name": "Sidi abdallah Mosque",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJSZ0E6_-yjxIRALgWiT5Wtc8",
            "google_id": "b8a608852557552d4df2cc5cf30c74a182149089",
            "photo_attribution": "b8a608852557552d4df2cc5cf30c74a182149089",
            "google_rating": 4.2,
            "google_ratings_total": 18,
            "vicinity": "Rue Mustapha Latreche, Casbah",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/109288344694860162442\">CHERA AHMED MILOUD</a>"
            ],
            "image_reference": "CmRaAAAAImx6rCPxR2Rm1yyVESCi_iclSVDgNn2-hDns_BNVE-DL8detvfaFjH-yAkVPRGmWqysaA7h1I62xknXktWkCzOQKYMyU9prRrflotdKT8aq1_6HkLYzExiNjWuHYsV40EhDhLMlnxGtgJv0pEn9x5qm8GhShByotMb19wJzwwFAvYwQDthqYwQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.78505039999999,
            "lon": 3.0614747,
            "name": "\u062f\u0627\u0631 \u0639\u0632\u064a\u0632\u0629",
            "types": [
                "museum",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJGYGRF_6yjxIR7A4sjtn77T8",
            "google_id": "33fe67da9c28fc22feb4969743180b63450fa150",
            "photo_attribution": "33fe67da9c28fc22feb4969743180b63450fa150",
            "google_rating": 4.2,
            "google_ratings_total": 54,
            "vicinity": "Dar Aziza 02, place Cheikh Ben Badis-, Casbah",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/109086548127440517791\">imed lakhal</a>"
            ],
            "image_reference": "CmRaAAAADgvuSUjF58BQIVubnmJrKkNbpSK1VZoBo03DUpi5jm4q1HhAhtOC6O2YvKcHnEeBr9YUakDDU-0xQUXTyuJoGRskLAKd_JAYDaD2uE6F3HV8cuwC3OSJAruKFzAh3qlwEhCGebMF3jysW7SEiBa_4qaNGhRs1DNAQJuzcw1DW0rb-wxC1QlepA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7849783,
            "lon": 3.0630197,
            "name": "Djemaa El Djedid",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJU1zj5P2yjxIRI6skzUmXqUw",
            "google_id": "5a662fd0e8cb9975dba48cdf2221dd4d96c84007",
            "photo_attribution": "5a662fd0e8cb9975dba48cdf2221dd4d96c84007",
            "google_rating": 4.7,
            "google_ratings_total": 66,
            "vicinity": "Boulevard Amilcar Cabral, \u0627\u0644\u062c\u0632\u0627\u0626\u0631",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/114496320266697690710\">Naseem DDnho</a>"
            ],
            "image_reference": "CmRaAAAArOFMQzIUjJmZ7VSC3nOg4UNnG6qPtncxLhIH_kWKbWxOJqnQixoDz96W5eOJ6Mnliz-vYrFv2-A56dsfeiVyjgCfzk8bgefBumfIR8l07tEn3YDhZa3OeBMlIpOJt-NYEhDKyrE_202PNV_y06UWo1kZGhS9_QY5QTM7NVCquzTyQjhaJfICPQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7852437,
            "lon": 3.0608675,
            "name": "Dar Hassan Pacha",
            "types": [
                "tourist_attraction",
                "premise",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ0U8AJf6yjxIRk-x--4UblRI",
            "google_id": "7399a519084ebe42823ef2054033765ce1d5a5a1",
            "photo_attribution": "7399a519084ebe42823ef2054033765ce1d5a5a1",
            "google_rating": 4.4,
            "google_ratings_total": 30,
            "vicinity": "Algiers",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/110227667278285789034\">Omar Hamdi</a>"
            ],
            "image_reference": "CmRaAAAAV3KD_Vykw71hNwtNoFNQJmVa8emXtVJEyEM3wOfriNUopekuMP6LYlPMbL27GbZK2JKB-6g3LT_H75dVpBtcQlQABPh3ZOC5uGjhg6je-Pz-1p9Nk-a1NxR_gUuxLj-CEhDRT1WWb5B5niZXqn6UTuBlGhSEHfFtYkVXOunfVrNfvdj_GEBIJA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7864516,
            "lon": 3.058565900000001,
            "name": "Sidi Ramadan",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJWbr1GQCzjxIR7kXA_L7QqdY",
            "google_id": "3c6b690f2c55c3e0b6f6e8de82dad2180833a745",
            "photo_attribution": "3c6b690f2c55c3e0b6f6e8de82dad2180833a745",
            "google_rating": 4.3,
            "google_ratings_total": 47,
            "vicinity": "Alger",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/112595828643362510650\">Chris Hunkeler</a>"
            ],
            "image_reference": "CmRaAAAA5mDybrlAAMFSIzjNtYKkF2AFwlIJuKgfAVlJdgIt5iLSzvTzMLwG7pCLIYBAOOF1jrQ-vWf2qebAb4zz-gWF24Lw2H7S2HNOu6nARJoi3X_ojC91ylMHG76TqcjpRIYIEhAq-e2g7NtCvbQQ807kMltUGhTx_uCmttGOdsvspf_Gkm-EC4eaEQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.78703369999999,
            "lon": 3.0619106,
            "name": "Ali Bitchin Mosque",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ_1s-7QGzjxIRLDvkodj7Tu8",
            "google_id": "1c9fea280500ab2a4f4d5de3c261dab16c088f3e",
            "photo_attribution": "1c9fea280500ab2a4f4d5de3c261dab16c088f3e",
            "google_rating": 4.4,
            "google_ratings_total": 24,
            "vicinity": "Rue Pr Mohamed SOUALAH, Casbah",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/101386642313114887093\">Abdelilah Boubchir</a>"
            ],
            "image_reference": "CmRaAAAAk3PvJkmT-H1Ucc2cG8ko0RJcSQC50tHP0BaMxFZdV1wXQ0zlcF7g3_2iOmYExykygh3YCUhMED3o3GphXIx8j5Wikl1JfwBM6iHZavIO5MJPg-7UdZCPhMH7hi206pxYEhDGCEAbMgBA0kXPKS4_-sGSGhTwg6jYwnBdBQ1dRhQKN2o3Xebi5A"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7890322,
            "lon": 3.0590285,
            "name": "Prague Garden",
            "types": [
                "tourist_attraction",
                "park",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJgY9BjQCzjxIRpcnTltGUKRk",
            "google_id": "04bffbadb7fa3d40b701766100b7fbce17b90762",
            "photo_attribution": "04bffbadb7fa3d40b701766100b7fbce17b90762",
            "google_rating": 4.3,
            "google_ratings_total": 55,
            "vicinity": "Bab El Oued",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/114684740497715250193\">Walid Oubbiche</a>"
            ],
            "image_reference": "CmRaAAAA20nIzalxfwlBsl3KGsmxVccR5LRCD1MwZbdIwr30Zp3j8rWST305EF2UjyS4p-2jZQFsC-2OUqGnyJ4dyNSH35AdsOexWhAUPhLjNeskgw2pruPZiR-XO3ORuHBpf9rvEhCEVyeYahMN2aQAv889XJipGhSZs7FRHUoFTM9Sen0vCbEoZOJx9w"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.79101000000001,
            "lon": 3.0518628,
            "name": "Trois Horloges",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJlbl4G6yzjxIR-84L_m6vUHE",
            "google_id": "0bd86a380f71b4c7a67578535ae1f65d8151edc3",
            "photo_attribution": "0bd86a380f71b4c7a67578535ae1f65d8151edc3",
            "google_rating": 4.1,
            "google_ratings_total": 284,
            "vicinity": "Bab El Oued",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/114201957302057593871\">BEKRETOU Mahdi</a>"
            ],
            "image_reference": "CmRaAAAA7iinD4kl8QnEWTqizFxra1YwAsQ3iVMfU647YwXaUTUuvT668Evxd8v4csmlrp5lShfjLAEz_Nqsi9A67bbNavbgLjogL9ufPz62H3kAyJGmAy3cYbPDV2iEZS2mAsTzEhA6-ZFQ6xpH4VWWpaJBN81UGhRf78l-rOTFwZvqNjMFpDL5CizeOQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7926685,
            "lon": 3.059448,
            "name": "Parc de jeux el kittani",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJT1Tzm2uzjxIRCc-FR1tzWw8",
            "google_id": "217e2610c75907b78a78bdd9d61399b755cdae67",
            "photo_attribution": "217e2610c75907b78a78bdd9d61399b755cdae67",
            "google_rating": 5,
            "google_ratings_total": 3,
            "vicinity": "Dar El Hadj omar, Rue Kadri Ali, Bab El Oued",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/108998698059408841534\">Amar Bouzenoune</a>"
            ],
            "image_reference": "CmRaAAAAyKhfeeL5_ZcxGpLal7SAsiAaDLbQsN93bYwyT_2qvhvLih60z0l_w2UCIFue87_9ms3s6Lxw7AYZ6i6QO7wY8po79pUdiGi2FMpweN_Bx1l6ZvyisUvaOAKgtOKmLpEIEhA8VAhvwjiAyjKOQ7xD8DaVGhQ2h3ObBYIcONs4TubDVg9mc7yErQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.74227080000001,
            "lon": 3.1225878,
            "name": "Le Poney de la sablette",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJtSrjhfhNjhIRe58Y7fGCZ4A",
            "google_id": "1282e8dac9ced414db73c79df0d41231af78da6c",
            "photo_attribution": "1282e8dac9ced414db73c79df0d41231af78da6c",
            "google_rating": null,
            "google_ratings_total": null,
            "vicinity": "Promenade des Sablettes, Hussein Dey",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/117387726610826032196\">Mohammed Boukhris</a>"
            ],
            "image_reference": "CmRaAAAAgm8QuZnO9z2Dj0wyPmBDCoFrDDHo2s04zinZaTXDIznUVT5z9gqTzTQMUHf3KGMBExWfLAT2-IMkYmRG9cHQEIap3RGgYkCZvOmDHadf3jAepRt8RE5d0XvU0hj0CAHNEhAW4yazDJaubIy4A58uPcwrGhQ_tC49a965GDizWKoNNJ04WSBzmA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7357791,
            "lon": 3.1379532,
            "name": "Algiers Great Mosque",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJbcKe3X1SjhIRBoSa6JtXOrE",
            "google_id": "a06cd4b5b80266711f1205fbc2e5b034abd0b1a1",
            "photo_attribution": "a06cd4b5b80266711f1205fbc2e5b034abd0b1a1",
            "google_rating": 4.5,
            "google_ratings_total": 1023,
            "vicinity": "Pins Martimes El Mohammadia, Mohammadia",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/108698803248999847092\">ismail djennane</a>"
            ],
            "image_reference": "CmRaAAAAfzWhA6jPPjahJfBNRhs4Q-eGZvKg2f-5-Q2Udgua_F_0jQjy5O-0dN0vQCOZkcRc9wkVTkN205oIOlXOh3SgAc9KRdIBl5CjHF0A-YsG1dkdC33kxpkNI7CGviIzqu-jEhA5ibUR_r3wa-rlkEU_R26AGhREy0COqgro6KUUGbomthlBakcxuw"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.6576027,
            "lon": 3.1044378,
            "name": "SPECIGET PARC",
            "types": [
                "park",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ3SJ1W1ZTjhIRZ3XQLpxaimw",
            "google_id": "4594e78c005a960a650ecc8b52cda932fb6d8692",
            "photo_attribution": "4594e78c005a960a650ecc8b52cda932fb6d8692",
            "google_rating": 4.6,
            "google_ratings_total": 7,
            "vicinity": "HAOUCHE HAMZA LOT N\u00b0 07 BARAKI ALGER (EN FACE 13 HECTARE), 13 HECTARES, Baraki",
            "image_attribution": null,
            "image_reference": null
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.74901029999999,
            "lon": 3.1879287,
            "name": "Fort Turc",
            "types": [
                "museum",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJJWEfSwVOjhIRZxvml0i50dk",
            "google_id": "30ef822fb25eca39cdec199b68dd51c5caa0bcd3",
            "photo_attribution": "30ef822fb25eca39cdec199b68dd51c5caa0bcd3",
            "google_rating": 3.9,
            "google_ratings_total": 10,
            "vicinity": "Bordj El Kiffan",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/105248365126229554644\">amin serir</a>"
            ],
            "image_reference": "CmRZAAAAZ74XeovLvj228DR8G14VZNjBzTB5dVpc5Ftlue_AzQTKhLQ3lENT5EoWhgDQ7PWQh84yYjKBjwvpQeTJVwsYgm6BIoVqFm_nn58pTc43JpQrpbrLTkvaXBlRHIhJ2SUOEhBIKhlLcoLLlXriFITSWo39GhSA1E9YvZnhxfEk4_-Y0655qvn8VA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.80734249999999,
            "lon": 3.2307436,
            "name": "Mus\u00e9e de Tamentfoust - Le Fort Turc",
            "types": [
                "museum",
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ1f6NYjxPjhIR3O6QnRNHG7E",
            "google_id": "0b86009089d410909117fd48b69be9d5a19a39e5",
            "photo_attribution": "0b86009089d410909117fd48b69be9d5a19a39e5",
            "google_rating": 4.2,
            "google_ratings_total": 58,
            "vicinity": "El Marsa",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/112485351759477220844\">guerroumi smail</a>"
            ],
            "image_reference": "CmRaAAAATTJF1yUa3cBcTzZ7VNzYihW1F6vP_8Ks3PWXWJsc43r9L5VAwy-17M99SusU6tDID4Qb8Www-Vtl2t0LeGZOMLLdWPtoa6viRfdxR8Fru3lIzbsA1-FFBXImhUIMNI36EhDpGUI6leUEdRF2y9J4HfYsGhQV-o8z9wlknvKQ4DBFY1hDWB_abQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.6789549,
            "lon": 2.8991777,
            "name": "Extreme Dz service agency",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJu3dPQtKljxIRDEPYMy_1iIQ",
            "google_id": "89924e68e54d10bc6298ec6febdad47ba380e4eb",
            "photo_attribution": "89924e68e54d10bc6298ec6febdad47ba380e4eb",
            "google_rating": 5,
            "google_ratings_total": 1,
            "vicinity": "CYBERPARC,SIDI ABDELLAH, Rahmania",
            "image_attribution": null,
            "image_reference": null
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7609037,
            "lon": 2.841145999999999,
            "name": "Plage Ouest",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJj3tFu7m8jxIRIno90KIKD1I",
            "google_id": "045d8ec4ff15a8ad25230802e7bd9929d696166f",
            "photo_attribution": "045d8ec4ff15a8ad25230802e7bd9929d696166f",
            "google_rating": 4,
            "google_ratings_total": 73,
            "vicinity": "Chemin de Thalassoth\u00e9rapie, \u0633\u0637\u0627\u0648\u0627\u0644\u064a",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/111441525966220416467\">CHENTOUT MALIKA</a>"
            ],
            "image_reference": "CmRaAAAAdZmFNzE_Lnn3DtRNNvaSVBL20nvLCLnExfQMmKcx0SQEsNHbYNvZ0ci6cE57ft2fYnZwH4qP-aM87AjF_yruG2CHMK0pD2lB2eiGSnZ7oDFHzegUKCFzqvqjsr3N4ne6EhBa1hjAvW9ijwKvoeP6oCeSGhRFCZsDB3qCGGgVxaNaFxpZD3Sofg"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7167159,
            "lon": 2.8258896,
            "name": "Plage Zeralda 1",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ_1Mt0BqjjxIRsQ659vzJ3xw",
            "google_id": "4cdddd9186365b7667029601e7d5d4644f141f19",
            "photo_attribution": "4cdddd9186365b7667029601e7d5d4644f141f19",
            "google_rating": 3.8,
            "google_ratings_total": 85,
            "vicinity": "Zeralda",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/114858189728553058845\">ABDERRAHIM BELAKRI</a>"
            ],
            "image_reference": "CmRaAAAAlBIW1AMA9T11eIukrbVuLKhd_F0vrR-g5wwcvcRJfZ9H8PAmKF-2Wd9xWgfwVmdrnmmfotCS-kn-_EpWFTqvP7L83kRQfsGDhCbPeZVNDy32TmDDrZmE5dJtFzQJiANbEhDhhEhbY9vmBU_9x5SwLJxjGhR0SqhW1SfuklkncIWbrfRSpDyoMA"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.686534,
            "lon": 2.7849282,
            "name": "Colonel Abbas",
            "types": [
                "tourist_attraction",
                "natural_feature",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJCdMx13KijxIRHCuqzy7G86s",
            "google_id": "80db7c059ea5581d0bd978cf721f2100def77d70",
            "photo_attribution": "80db7c059ea5581d0bd978cf721f2100def77d70",
            "google_rating": 3.8,
            "google_ratings_total": 208,
            "vicinity": "N11, Douaouda",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/106707713588087649316\">Fateh Khouas</a>"
            ],
            "image_reference": "CmRaAAAAm6Z41tGDZKm9zUiWlo0x9KoIjED1M-qT6NJuN-OF-5Z31ogbjKxlKv9rq764QWCdkA6xbRQtANgCr-g_Z2aJ3q10cYsYZhLZ91uNYR4Jp9n8nDNJtqZ5YeswapJcGbcDEhBQVlmAr0Z-6swIlwS78XobGhSWknV3PSJRYY8PJo1AoBF16ycslQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7335447,
            "lon": 3.4013844,
            "name": "Rahma mosque boudouaou",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJG9gxLbVCjhIR3WfLU5xqKSo",
            "google_id": "eb63222e073545c86ef12af3150c0137719f212b",
            "photo_attribution": "eb63222e073545c86ef12af3150c0137719f212b",
            "google_rating": 4.6,
            "google_ratings_total": 23,
            "vicinity": "Boudouaou",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/111018669200678569558\">amine Z k</a>"
            ],
            "image_reference": "CmRaAAAA3AaK19VglOZKFoTlCTqy27I93gDl4JbV1QiH3N1Rk3BX79Mf1Y7zb_buhaPZ51WyoSvr_gvXiKO247wyZ6U5NxVMvBvWKDKUqB1-xepnA-qPNH1_Y1RaXCYwwH6jCTALEhBDCMbsqoqnsoBkc7tvYUlcGhR0SkMTL7r6r6HL2zI9JmmYiqwC5A"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.427008,
            "lon": 2.985633,
            "name": "\u0634\u0644\u0627\u0644\u0627\u062a \u0645\u0642\u0637\u0639 \u0627\u0644\u0627\u0632\u0631\u0642",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJ71212lgFjxIRK9lsC4_2yGM",
            "google_id": "c79d46c55784372133acef85f356d8bd4687dae9",
            "photo_attribution": "c79d46c55784372133acef85f356d8bd4687dae9",
            "google_rating": 5,
            "google_ratings_total": 3,
            "vicinity": "\u062d\u0645\u0627\u0645 \u0645\u0644\u0648\u0627\u0646\u060c",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/100954548212889720750\">Mohamed Nacer Abdi \u0628\u0627\u062d\u0645\u064a\u0627 \u0627\u0644\u0645\u062a\u0641\u0627\u0626\u0644</a>"
            ],
            "image_reference": "CmRaAAAAwiFDkx-64v2O32I0p-QQ-dU09VQM3_EWTCnKl5a5gP_a31-ekJZU0tiSHAilsLT0M12lFXGOL0iNKu__NWVWcwFXnZ5y2R6adj969bXBOBehAWKTkaAfOf6HFmRN8yWXEhDzKQUDP-8ao_NH1r8WMfAzGhRSHgatBTWKMOp1uAHvXkJ9s3pkyQ"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.4725618,
            "lon": 2.8238216,
            "name": "Bader Mosque",
            "types": [
                "mosque",
                "tourist_attraction",
                "place_of_worship",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJDUJCvGYMjxIRDSVv-STctsc",
            "google_id": "10667f111667a0cb7a1ac50cde34b9c44be907cb",
            "photo_attribution": "10667f111667a0cb7a1ac50cde34b9c44be907cb",
            "google_rating": 4.4,
            "google_ratings_total": 30,
            "vicinity": "Blida",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/114496320266697690710\">Naseem DDnho</a>"
            ],
            "image_reference": "CmRaAAAA6-kkJ_SmryDE4YEm9N0ySTnZbno6yRe0hX6gxaVl0S-TYqovwbJp2GoQRvkGEybn9I_4AkevIqUoF5NRfbNiaALkdV59hcBPo-ZZkdZZrzn4gZHOuyunu4e6kOr9aBEaEhDDnPOFXBGAnO26jfEekKfJGhSjb0Eoym1wsWWLuWx7s22qkSPR1g"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.4336226,
            "lon": 2.9076094,
            "name": "\u0645\u0646\u062a\u0632\u0647 \u0627\u0644\u0634\u0631\u064a\u0639\u0629",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJd8A84ucPjxIRFTaXuUK2jug",
            "google_id": "215e6e56996648394a3675e8d302f854678a3fb2",
            "photo_attribution": "215e6e56996648394a3675e8d302f854678a3fb2",
            "google_rating": null,
            "google_ratings_total": null,
            "vicinity": "Chrea",
            "image_attribution": null,
            "image_reference": null
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.403785,
            "lon": 3.228198299999999,
            "name": "\u0634\u0644\u0627\u0644 \u0627\u0644\u0639\u064a\u0633\u0627\u0648\u064a\u0629",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJFUlZvj37jhIR34fWd8YEWWc",
            "google_id": "5b7fb441bd30e11617f475ffb548bfb002ab373d",
            "photo_attribution": "5b7fb441bd30e11617f475ffb548bfb002ab373d",
            "google_rating": 5,
            "google_ratings_total": 3,
            "vicinity": "Aissaouia",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/111392728767727108447\">Kader Sahali</a>"
            ],
            "image_reference": "CmRaAAAATbkRRMLkKvYcDWPvYuzMXuVVS5J5ecMZYTmeUnLrhLxQiTN_5ezA7W_3_o-_wxyDwZq9deWmW29_ysuWMXIAcfRtfaemrAaXzeQdi3pxbbgI9aWy-KHPo6nlWG43-KkrEhBIbfrI6tjLx95fikYbiF5MGhQR6G0vqQkqePof7NhbtPNdsUdZ-A"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.78694779999999,
            "lon": 3.5432625,
            "name": "Plage Sghirate",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJIQ3u4VhujhIRPgF7QTypTf4",
            "google_id": "3df08bfad2a1e3d28a7b458f8effeff921dc91d1",
            "photo_attribution": "3df08bfad2a1e3d28a7b458f8effeff921dc91d1",
            "google_rating": 4.2,
            "google_ratings_total": 135,
            "vicinity": "Th\u00e9nia",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/113037976424522989932\">chef DjediAhcen</a>"
            ],
            "image_reference": "CmRaAAAAXg2JYfgLlROogoXXDRt-Cj3hFA6HiePedFsvFK-0DYTxd76QbjMLyie2cMGp5Uxk25o5UldpjHuvoNRF9ZuUnjnZCqqPNE5Vr4PdnumuQsKhAHCFArNW2hBmA7rEOPXhEhCK6CnU5B-CUOMD8MwMfjU9GhSRfbUMKPvWsjKKSb4US7qwvA4Lfg"
        },
        {
            "business_status": "OPERATIONAL",
            "lat": 36.7864379,
            "lon": 3.5466337,
            "name": "Ushuaia Beach",
            "types": [
                "tourist_attraction",
                "point_of_interest",
                "establishment"
            ],
            "google_place_id": "ChIJi0CCz1tujhIRUuPRCWtOqeo",
            "google_id": "983ea399d0012c3cbed4ea3296e6e1d3e2f3725c",
            "photo_attribution": "983ea399d0012c3cbed4ea3296e6e1d3e2f3725c",
            "google_rating": 3.7,
            "google_ratings_total": 22,
            "vicinity": "sghirat",
            "image_attribution": [
                "<a href=\"https://maps.google.com/maps/contrib/115214980646147213253\">Abdelkrim HAMADACHE</a>"
            ],
            "image_reference": "CmRaAAAAj51znZV22s5rQXRs9sDisIN1DlmE4ZnsDggxQEzMIXs1K_zIDRQEioJp1V8Eg-WZPYuHUSVRWcXYYnjb5_O23DTD4GBlNUByknY0GO9iCQkbsMbCuOKY_yZm2XyhBh4CEhCteaA5YK6_gnuaQnTzOirFGhQYEFl22QjpwkIUDqwTCoeJoW62cQ"
        }
    ]
};